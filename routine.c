#include "c0init.h"

void sysprepare() {
  if (mount("proc", "/proc", "proc", MS_NOSUID | MS_NOEXEC | MS_NODEV, NULL)) {
    perror(CRED "mount /proc" CCLEAR);
  }

  if (mount("sysfs", "/sys", "sysfs", MS_NOSUID | MS_NOEXEC | MS_NODEV, NULL)) {
    perror(CRED "mount /sys" CCLEAR);
  }
  
  if (mount("devtmpfs", "/dev", "devtmpfs", MS_NOSUID, "mode=0755")) {
    perror(CRED "mount /dev" CCLEAR);
  }

  if (mkdir("/dev/pts", 0755) && errno != EEXIST) {
    perror(CRED "creating /dev/pts" CCLEAR);
  }

  if (mount("devpts", "/dev/pts", "devpts", MS_NOSUID | MS_NOEXEC, "gid=5,mode=0620")) {
    perror(CRED "mount /dev/pts" CCLEAR);
  }

  if (mount("shm", "/dev/shm", "tmpfs", MS_NOSUID | MS_NOEXEC, NULL)) {
    perror(CRED "mount /dev/shm" CCLEAR);
  }

  if (mount(NULL, "/", NULL, MS_REMOUNT, "rw")) {
    perror(CRED "remount / as rw" CCLEAR);
  }
}

void syshostname() {
  FILE *fp;
  char hostname[256];

  fp = fopen("/etc/hostname", "r");
  if (!fp) {
    perror(CRED "open /etc/hostname" CCLEAR);
    return;
  }

  if (!fgets(hostname, sizeof(hostname), fp)) {
    perror(CRED "read /etc/hostname" CCLEAR);
    fclose(fp);
    return;
  }

  size_t len = strlen(hostname);
  if (hostname[len - 1] == '\n') {
    hostname[len - 1] = '\0';
  }

  if (sethostname(hostname, strlen(hostname)) == 0) {
    printf(CGRAY "hostname set to %s" CCLEAR "\n", hostname);
  } else {
    perror(CRED "sethostname" CCLEAR);
  }

  fclose(fp);
}

void sysnetwork() {
  int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sockfd == -1) {
    perror(CRED "socket" CCLEAR);
    return;
  }

  struct sockaddr_in sa;
  struct ifreq ifr;

  memset(&ifr, 0, sizeof(ifr));
  strncpy(ifr.ifr_name, "lo", IFNAMSIZ - 1);

  sa.sin_family = AF_INET;
  sa.sin_addr.s_addr = inet_addr("127.0.0.1");
  memcpy(&ifr.ifr_addr, &sa, sizeof(struct sockaddr));

  if (ioctl(sockfd, SIOCSIFADDR, &ifr) == -1) {
    perror(CRED "ioctl SIOCSIFADDR" CCLEAR);
    close(sockfd);
    return;
  }

  memset(&ifr, 0, sizeof(ifr));
  strncpy(ifr.ifr_name, "lo", IFNAMSIZ - 1);
  ifr.ifr_flags = IFF_UP;

  if (ioctl(sockfd, SIOCSIFFLAGS, &ifr) == -1) {
    perror(CRED "ioctl SIOCSIFFLAGS" CCLEAR);
    close(sockfd);
    return;
  }

  close(sockfd);
}

void sysrandomseed() {
  FILE *fsrc = fopen("/etc/randomseed", "rb");
  if (!fsrc) {
    perror(CRED "open /etc/randomseed" CCLEAR);
    return;
  }

  FILE *fdst = fopen("/dev/urandom", "wb");
  if (!fdst) {
    perror(CRED "open /dev/urandom" CCLEAR);
    fclose(fsrc);
    return;
  }

  char buf[4096];
  size_t len = fread(buf, 1, sizeof(buf), fsrc);

  if (len <= 0) {
    perror(CRED "read /etc/randomseed" CCLEAR);
    fclose(fsrc);
    fclose(fdst);
    return;
  }

  size_t wrt = fwrite(buf, 1, len, fdst);
  if (wrt == len) {
    printf(CGRAY "seeded with %ld bits of entropy" CCLEAR "\n", wrt * 8);
  } else {
    perror(CRED "write /dev/urandom" CCLEAR);
  }

  fclose(fsrc);
  fclose(fdst);
}

void sysrandomentropy() {
  FILE *fsrc = fopen("/dev/urandom", "rb");
  if (!fsrc) {
    perror(CRED "open /dev/urandom" CCLEAR);
    return;
  }

  FILE *fdst = fopen("/etc/randomseed", "wb");
  if (!fdst) {
    perror(CRED "open /etc/randomseed" CCLEAR);
    fclose(fsrc);
    return;
  }

  char buf[32];
  size_t len = fread(buf, 1, sizeof(buf), fsrc);
  if (len != 32) {
    perror(CRED "read discard /dev/urandom" CCLEAR);
    fclose(fsrc);
    fclose(fdst);
    return;
  }

  len = fread(buf, 1, sizeof(buf), fsrc);
  if (len != 32) {
    perror(CRED "read entropy /dev/urandom" CCLEAR);
    fclose(fsrc);
    fclose(fdst);
    return;
  }

  size_t wrt = fwrite(buf, 1, len, fdst);
  if (wrt == len) {
    printf(CGRAY "stored %ld bits of entropy for next boot" CCLEAR "\n", wrt * 8);
  } else {
    perror(CRED "write /etc/randomseed" CCLEAR);
  }

  fclose(fsrc);
  fclose(fdst);
}

void systasks() {
  const char *dirpath = "/etc/task.d";
  DIR *dir = opendir(dirpath);

  if (!dir) {
    perror(CRED "opendir /etc/task.d/" CCLEAR);
    return;
  }

  struct dirent *entry;
  while ((entry = readdir(dir)) != NULL) {
    struct stat fstat;
    char fpath[256];
    snprintf(fpath, sizeof(fpath), "%s/%s", dirpath, entry->d_name);

    if (stat(fpath, &fstat) == 0 && S_ISREG(fstat.st_mode) && (fstat.st_mode & S_IXUSR)) {
      if (entry->d_name[0] != '-') {
        char *const argv[] = {fpath, NULL};
#ifdef C0INIT_ASYNC
        spawn(argv);
#else
        spawnwait(argv);
#endif
      }
    }
  }

  closedir(dir);
}

void sysearlyboot() {
  const char *dirpath = "/etc/task.d/earlyboot";
  DIR *dir = opendir(dirpath);

  if (!dir) {
    perror(CRED "opendir /etc/task.d/earlyboot" CCLEAR);
    return;
  }

  struct dirent *entry;
  while ((entry = readdir(dir)) != NULL) {
    struct stat fstat;
    char fpath[256];
    snprintf(fpath, sizeof(fpath), "%s/%s", dirpath, entry->d_name);

    if (stat(fpath, &fstat) == 0 && S_ISREG(fstat.st_mode) && (fstat.st_mode & S_IXUSR)) {
      if (entry->d_name[0] != '-') {
        char *const argv[] = {fpath, NULL};
#ifdef C0INIT_ASYNC
        spawn(argv);
#else
        spawnwait(argv);
#endif
      }
    }
  }

  closedir(dir);
}

void syspoweroff() {
  puts(CYELLOW "running poweroff scripts..." CCLEAR);

  const char *dirpath = "/etc/poweroff.d";
  DIR *dir = opendir(dirpath);

  if (!dir) {
    perror(CRED "opendir /etc/poweroff.d" CCLEAR);
    return;
  }

  struct dirent *entry;
  while ((entry = readdir(dir)) != NULL) {
    struct stat fstat;
    char fpath[256];
    snprintf(fpath, sizeof(fpath), "%s/%s", dirpath, entry->d_name);

    if (stat(fpath, &fstat) == 0 && S_ISREG(fstat.st_mode) && (fstat.st_mode & S_IXUSR)) {
      if (entry->d_name[0] != '-') {
        char *const argv[] = {fpath, NULL};
        spawnwait(argv);
      }
    }
  }

  closedir(dir);
}

void killall5(int sig) {
  printf(CYELLOW "sending %s to all processes\n" CCLEAR, strsignal(sig));
  DIR *dir = opendir("/proc");

  if (!dir) {
    perror(CRED "opendir /proc");
    return;
  }

  struct dirent *entry;
  pid_t pid;
  while ((entry = readdir(dir)) != NULL) {
    if ((pid = strtol(entry->d_name, NULL, 10)) <= 0) {
      continue;
    }

    if (pid == 1 || getsid(pid) == getsid(0) || getsid(pid) == 0) {
      continue;
    }

    printf("killing %d with %s\n", pid, strsignal(sig));
    kill(pid, sig);
  }

  closedir(dir);
}


void sysservices() {
  // TODO start enabled services
}

void killsvc() {
  puts(CYELLOW "stopping all services" CCLEAR);
  // TODO stop/kill running services
}
