#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/mount.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <errno.h>
#include <string.h>
#include <mntent.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <dirent.h>
#include <limits.h>
#include <sys/swap.h>
#include <sys/syscall.h>
#include <linux/reboot.h>

#include "config.h"

#define reboot(cmd) syscall(SYS_reboot, LINUX_REBOOT_MAGIC1, LINUX_REBOOT_MAGIC2, cmd, NULL)

void sysprepare();
void sysearlyboot();
void mountfstab();
void syshostname();
void sysnetwork();
void sysrandomseed();
void sysrandomentropy();
void systasks();
void sysservices();

void syspoweroff();
void killsvc();
void killall5(int);

void sigpoweroff();
void sigreap();
void sigreboot();

void spawnwait(char *const*);
void spawn(char *const*);

void parseopts(const char*, unsigned long*, char*, size_t);
char *devname_from_label(const char*);
char *devname_from_uuid(const char*);
void resolve_mount(char**);

#ifdef C0INIT_COLORS
#define CGREEN "\e[32m"
#define CRED "\e[91m"
#define CYELLOW "\e[93m"
#define CGRAY "\e[90m"
#define CCLEAR "\e[0m"
#else
#defube CGREEN ""
#defube CRED ""
#defube CYELLOW ""
#defube CGRAY ""
#defube CCLEAR ""
#endif
