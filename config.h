// c0init

// when defined, c0init will not wait for tasks to finish before starting the next one
#define C0INIT_ASYNC

// when defined, c0init output will use ANSI escape codes
#define C0INIT_COLORS

// when defined, c0init may use insults in messages
#define C0INIT_INSULTS

// how long (in seconds) to wait between poweroff stages
#define C0INIT_POWEROFF_PERIOD 2

// scripts in /etc/task.d/earlyboot/ will run after system preparations
// scripts in /etc/task.d/ will run before services
// scripts in /etc/svc.d/ will be kept alive forever
// scripts in /etc/poweroff.d/ will run synchronously before poweroff
