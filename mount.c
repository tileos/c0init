#include "c0init.h"

#define FSOPTS_MAXLEN 512
struct {
  const char *opt;
  const char *notopt;
  unsigned long v;
} optnames[] = {
  { "defaults",   NULL,           0              },
  { "remount",    NULL,           MS_REMOUNT     },
  { "ro",         "rw",           MS_RDONLY      },
  { "sync",       "async",        MS_SYNCHRONOUS },
  { "dirsync",    NULL,           MS_DIRSYNC     },
  { "nodev",      "dev",          MS_NODEV       },
  { "noatime",    "atime",        MS_NOATIME     },
  { "noauto",     "auto",         0              },
  { "nodiratime", "diratime",     MS_NODIRATIME  },
  { "noexec",     "exec",         MS_NOEXEC      },
  { "nosuid",     "suid",         MS_NOSUID      },
  { "mand",       "nomand",       MS_MANDLOCK    },
  { "relatime",   "norelatime",   MS_RELATIME    },
  { "bind",       NULL,           MS_BIND        },
  { NULL,         NULL,           0              }
};

void mountfstab() {
  FILE *fstab;
  struct mntent *entry;

  static unsigned long argflags = 0;
  static char fsopts[FSOPTS_MAXLEN] = "";

  fstab = setmntent("/etc/fstab", "r");
  if (!fstab) {
    perror(CRED "open /etc/fstab" CCLEAR);
    return;
  }

  while ((entry = getmntent(fstab)) != NULL) {
    char *src = entry->mnt_fsname;
    const char *targ = entry->mnt_dir;
    const char *fstype = entry->mnt_type;
    const char *opts = entry->mnt_opts;

    resolve_mount(&src);

    if (!strcmp(fstype, "auto")) {
      fstype = NULL;
    }

    if (!strcmp(fstype, "swap")) {
      if (swapon(src, 0) < 0) {
        printf(CRED "swapon %s" CCLEAR ": ", src);
        fflush(stdout);
        perror("");
      }
      continue;
    }

    parseopts(opts, &argflags, fsopts, FSOPTS_MAXLEN);

    if (mount(src, targ, fstype, argflags, fsopts) != 0) {
      printf(CRED "mount %s" CCLEAR ": ", targ);
      fflush(stdout);
      perror("");
      printf("%s %s %s %ld %s (%s)\n", src, targ, fstype, argflags, fsopts, opts);
    }
  }

  endmntent(fstab);

  if (symlink("/proc/mounts", "/etc/mtab") && errno != EEXIST) {
    perror(CRED "/proc/mounts -> /etc/mtab" CCLEAR);
  }
}

// taken from suckless ubase mount.c
void parseopts(const char *popts, unsigned long *flags, char *data, size_t datasiz) {
  unsigned int i, validopt;
  size_t optlen, dlen = 0;
  const char *name, *e;
 
  name = popts;
  data[0] = '\0';
  do {
    if ((e = strstr(name, ",")))
      optlen = e - name;
    else
      optlen = strlen(name);
 
    validopt = 0;
    for (i = 0; optnames[i].opt; i++) {
      if (optnames[i].opt &&
          !strncmp(name, optnames[i].opt, optlen)) {
        *flags |= optnames[i].v;
        validopt = 1;
        break;
      }
      if (optnames[i].notopt &&
          !strncmp(name, optnames[i].notopt, optlen)) {
        *flags &= ~optnames[i].v;
        validopt = 1;
        break;
      }
    }
 
    if (!validopt && optlen > 0) {
      /* unknown option, pass as data option to mount() */
      if (dlen + optlen + 2 >= datasiz)
        return; /* prevent overflow */
      if (dlen)
        data[dlen++] = ',';
      memcpy(&data[dlen], name, optlen);
      dlen += optlen;
      data[dlen] = '\0';
    }
    name = e + 1;
  } while (e);
}

void resolve_mount(char **fsname) {
  char *tmp = *fsname;

  if (strstr(tmp, "UUID=") == tmp) {
    tmp = devname_from_uuid(*fsname + 5);
  } else if (strstr(tmp, "LABEL=") == tmp) {
    tmp = devname_from_label(*fsname + 6);
  }

  if (tmp)
    *fsname = tmp;
}

char *devname_from_label(const char *label) {
  char path[PATH_MAX];
  snprintf(path, sizeof(path), "/dev/disk/by-label/%s", label);

  return realpath(path, NULL);
}

char *devname_from_uuid(const char *uuid) {
  char path[PATH_MAX];
  snprintf(path, sizeof(path), "/dev/disk/by-uuid/%s", uuid);

  return realpath(path, NULL);
}

