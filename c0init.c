#define C0INIT_CONFIG
#include "c0init.h"

#define LEN(x)	(sizeof (x) / sizeof *(x))
#define TIMEO	30

static struct {
  int sig;
  void (*handler)(void);
} sigmap[] = {
  { SIGUSR1, sigpoweroff },
  { SIGCHLD, sigreap     },
  { SIGALRM, sigreap     },
  { SIGINT,  sigreboot   },
};

sigset_t set;

int main(int argc, char **argv) {
  if (getpid() != 1) {
#ifdef C0INIT_INSULTS
    puts(CRED "c0init: need pid 1 idiot" CCLEAR);
#else
    puts(CRED "c0init: must run as PID 1" CCLEAR);
#endif
    return 1;
  }
  chdir("/");
  umask(022);

  puts(CYELLOW "================");
  puts(        "= c0init 0.1.0 =");
  puts(        "================\n" CCLEAR);

  puts(CGREEN "preparing system" CCLEAR);
  sysprepare();

  puts(CGREEN "running earlyboot tasks" CCLEAR);
  sysearlyboot();

  puts(CGREEN "mounting filesystems" CCLEAR);
  mountfstab();

  puts(CGREEN "setting hostname" CCLEAR);
  syshostname();

  puts(CGREEN "bringing up lo interface" CCLEAR);
  sysnetwork();

  puts(CGREEN "setting random seed" CCLEAR);
  sysrandomseed();

  puts(CGREEN "running tasks" CCLEAR);
  systasks();

  puts(CGREEN "starting services" CCLEAR);
  sysservices();

  int sig;
  sigfillset(&set);
  sigprocmask(SIG_BLOCK, &set, NULL);
  while (1) {
    alarm(TIMEO);
    sigwait(&set, &sig);
    for (size_t i = 0; i < LEN(sigmap); i++) {
      if (sigmap[i].sig == sig) {
        sigmap[i].handler();
        break;
      }
    }
  }
#ifdef C0INIT_INSULTS
  puts(CRED "oh well you fucked up cuz init died just now" CCLEAR);
#else
  puts(CRED "init died. brace for impact!" CCLEAR);
#endif

  // try to prevent disaster
  sync();
  sleep(C0INIT_POWEROFF_PERIOD);
  return 0;
}


void sigpoweroff() {
  puts(CYELLOW "storing random entropy for next boot" CCLEAR);
  sysrandomentropy();

  killsvc();
  syspoweroff();
  
  killall5(SIGTERM);
  sleep(C0INIT_POWEROFF_PERIOD);
 
  killall5(SIGKILL);
  sleep(C0INIT_POWEROFF_PERIOD);

#ifdef C0INIT_INSULTS
  puts(CYELLOW "preventing your files from going kaboom" CCLEAR);
#else
  puts(CYELLOW "syncing I/O operations" CCLEAR);
#endif
  sync();
  sleep(C0INIT_POWEROFF_PERIOD);

#ifdef C0INIT_INSULTS
  puts(CYELLOW "cya later nerd" CCLEAR);
#else
  puts(CYELLOW "performing system poweroff" CCLEAR);
#endif
  reboot(LINUX_REBOOT_CMD_POWER_OFF);
}

void sigreap() {
  while (waitpid(-1, NULL, WNOHANG) > 0)
    ;
  alarm(TIMEO);
}

void sigreboot() {
  puts(CYELLOW "storing random entropy for next boot" CCLEAR);
  sysrandomentropy();

  killsvc();
  syspoweroff();
  
  killall5(SIGTERM);
  sleep(C0INIT_POWEROFF_PERIOD);
 
  killall5(SIGKILL);
  sleep(C0INIT_POWEROFF_PERIOD);

#ifdef C0INIT_INSULTS
  puts(CYELLOW "preventing your files from going kaboom" CCLEAR);
#else
  puts(CYELLOW "syncing I/O operations" CCLEAR);
#endif
  sync();
  sleep(C0INIT_POWEROFF_PERIOD);

#ifdef C0INIT_INSULTS
  puts(CYELLOW "cya soon nerd" CCLEAR);
#else
  puts(CYELLOW "performing system reboot" CCLEAR);
#endif
  reboot(LINUX_REBOOT_CMD_RESTART);
}

void spawn(char *const *argv) {
  switch (fork()) {
  case 0:
    sigprocmask(SIG_UNBLOCK, &set, NULL);
    setsid();
    execvp(argv[0], argv);
    perror("execvp");
    _exit(1);
  case -1:
    perror("fork");
  }
}

void spawnwait(char *const *argv) {
  int pid = fork();
  switch (pid) {
  case 0:
    sigprocmask(SIG_UNBLOCK, &set, NULL);
    setsid();
    execvp(argv[0], argv);
    perror("execvp");
    _exit(1);
  case -1:
    perror("fork");
  default:
    if (waitpid(pid, NULL, 0) == -1)
      perror("waitpid");
  }
}
