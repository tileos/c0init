c0init: c0init.c routine.c mount.c
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

install: c0init
	cp c0init $(DESTDIR)/sbin/c0init
	ln -s /sbin/c0init $(DESTDIR)/sbin/init
	mkdir -p $(DESTDIR)/etc/task.d/earlyboot
	mkdir -p $(DESTDIR)/etc/svc.d

.PHONY: install
